//
// MRPGv2::Framework
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/26 kanglib
//

#include "MRPG.h"

Framework::Framework(std::string title, int w, int h) : w(w), h(h), fps(MAX_FPS)
{
    if (SDL_Init(SDL_INIT_VIDEO)) {
        log("SDL_Init");
        throw 1;
    }

    if (TTF_Init()) {
        log("TTF_Init");
        throw 1;
    }

    win = SDL_CreateWindow(
            title.c_str(),
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            w,
            h,
            0);
    if (!win) {
        log("SDL_CreateWindow");
        throw 1;
    }

    ren = SDL_CreateRenderer(
             win,
            -1,
            SDL_RENDERER_SOFTWARE);
    if (!ren) {
        log("SDL_CreateRenderer");
        throw 1;
    }
}

Framework::~Framework()
{
    IMG_Quit();
    if (ren) {
        SDL_DestroyRenderer(ren);
    }
    if (win) {
        SDL_DestroyWindow(win);
    }
    if (TTF_WasInit()) {
        TTF_Quit();
    }
    if (SDL_WasInit(SDL_INIT_EVERYTHING)) {
        SDL_Quit();
    }
}

void Framework::log(std::string tag)
{
    std::cerr << tag << ": " << SDL_GetError() << std::endl;
}

void Framework::wndProc(EVENT_HANDLER handle, RENDERER render, ENTER_FRAME enterFrame)
{
    const int MAX_DT = (1000 / MAX_FPS) + 1;
    for (;;) {
        int t_i = SDL_GetTicks();

        SDL_RenderClear(ren);
        enterFrame(ren);

        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            if (!handle(e)) {
                return;
            }
        }

        render(ren);
        SDL_RenderPresent(ren);

        int dt = SDL_GetTicks() - t_i;
        if (dt < MAX_DT) {
            SDL_Delay(MAX_DT - dt);
            dt = SDL_GetTicks() - t_i;
        }
        fps = 1000 / dt;

        e.type = SDL_KEYDOWN;
        e.key.keysym.sym = SDLK_PASTE;
        SDL_PushEvent(&e);
    }
}

SDL_Texture* Framework::loadTexture(std::string image)
{
    SDL_Texture* tex = IMG_LoadTexture(ren, image.c_str());

    if (!tex) {
        log("IMG_LoadTexture");
        throw 1;
    }
    return tex;
}

void Framework::render(SDL_Texture* tex, int x, int y, SDL_Rect* clip)
{
    SDL_Rect dst;
    dst.x = x;
    dst.y = y;
    if (clip) {
        dst.w = clip->w;
        dst.h = clip->h;
    } else {
        SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
    }
    SDL_RenderCopy(ren, tex, clip, &dst);
}

void Framework::render(SDL_Surface * sur, int x, int y, SDL_Rect* clip)
{
    SDL_Texture* tex = SDL_CreateTextureFromSurface(ren, sur);
    render(tex, x, y, clip);
    SDL_DestroyTexture(tex);
}
