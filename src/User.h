//
// MRPGv2::User
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/26 kanglib
//

#pragma once

class User {
    Map* map;
    DB *db;
    std::string name;
    std::string color;
    int x, y;
    int speed;
    float stamina, MAX_STAMINA;
    int movState;
    int money = 0;
    enum { UP, LEFT, DOWN, RIGHT } direction;

    SDL_Texture* tex[4];
    SDL_Texture *invTex;
    std::vector<Item*> *inventory;
    std::map<int, Item*> *itemList;
    std::map<int, Quest*> *questList;
    std::vector<Quest*> *quests;

public:
    enum { STOP, WALKING, RUNNING, SWIMMING };

    User(Map* map, DB *db, std::string name, std::string color = "gray");
    ~User();

    std::string getName() { return name; }
    void getPosition(int& x, int& y) { x = this->x; y = this->y; }
    Map::Position getPosition() { Map::Position p; p.x = x; p.y = y; return p; }
    std::vector<Item *>* getInventory() { return new std::vector<Item*>(*inventory); }
    std::map<int, Item*>* getItemList() { return new std::map<int, Item*>(*itemList); }
    float maxStamina() { return MAX_STAMINA; }
    float getStamina() { return stamina; }
    int movingState() { return movState; }
    void resetState() { movState = STOP; }
    void recoverStamina(float s) { if (stamina < MAX_STAMINA) { stamina += s; } else { stamina = MAX_STAMINA; } }
    void setSpeed(int speed);
    int getMoney() { return money; }
    void addMoney(int money) { if (this->money + money >= 0) this->money += money; }

    void render(int x, int y);
    void move(float dx, float dy, float m = 1);

    SDL_Texture* getInvTex() { return invTex; }
    void initItems();
    bool extractItem(Item* &item);
    bool addItem(int num, int count);

    void initQuests();
    void checkQuests(Quest::Condition now);
    std::vector<Map::Mark*>* getQuestPlaces(Quest::Condition now);
    void renderQuests();
};
