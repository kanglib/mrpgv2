﻿//
// MRPGv2::Item
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/11 kanglib
//

#include "MRPG.h"

Item::Item(int num, int count, std::string name, int price, std::string imagePath, std::string description, Framework *framework)
    :num(num), count(count), name(name), price(price), imagePath(imagePath), description(description), framework(framework)
{
}

void Item::render(Font::FORMAT_DATA data) {
    char path[40];
    sprintf(path, "res/image/item/%s", imagePath.c_str());
    SDL_Texture *tex = framework->loadTexture(path);

    SDL_Texture *pad = framework->loadTexture("res/image/notepad.png");
    Font *Nanum20 = new Font(framework, "res/font/NanumBarunGothic.ttf", 20);
    Font *Nanum12 = new Font(framework, "res/font/NanumBarunGothic.ttf", 12);

    framework->render(tex, 220, 30);
    framework->render(pad, 500, 60);
    SDL_Color black = { 0, 0, 0 };

    vector<std::string> *desc = new vector<std::string>();
    int i = -1;
    int pos = description.find("\\n");
    if (pos > 0) {
        while (pos != string::npos) {
            desc->push_back(description.substr(i+1, pos - i - 1));
            i = ++pos;
            pos = description.find("\\n", pos+1);
            if (pos == string::npos) {
                desc->push_back(description.substr(i+1, description.length()));
            }
        }
    }
    else {
        desc->push_back(description);
    }

    wchar_t *encoded;
    Nanum20->encode(encoded, name.c_str());
    Nanum20->render(encoded, 540, 80, black);
    wchar_t countEncoded[10];
    swprintf(countEncoded, sizeof(countEncoded) / sizeof(wchar_t), L"%d개", count);
    Nanum12->render(countEncoded, 540, 100, black);

    i = 0;
    vector<std::string>::iterator it;
    for (it = desc->begin(); it != desc->end(); it++) {
        Nanum12->encode(encoded, (*it).c_str());
        Nanum12->format(encoded, data);
        Nanum12->render(encoded, 540, 112+12*(i++), black);
    }

    free(encoded);
    delete desc;

    framework->render(tex, 220, 30);
    SDL_DestroyTexture(pad);
    SDL_DestroyTexture(tex);

}
