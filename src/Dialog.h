//
// MRPGv2::Dialog
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/10 RangeWING
//

#pragma once

class Dialog {
    Framework *framework;

    Font *font;
    wchar_t msg[256];

public:
    Dialog(Framework *framework);
    ~Dialog();

    void setMessage(wchar_t *msg);
    void render();
};
