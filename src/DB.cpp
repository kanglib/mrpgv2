//
// MRPGv2::DB
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/12 RangeWING
//

#include "MRPG.h"

DB::DB(Framework *framework, bool init) : framework(framework) {
    xmlInv = new tinyxml2::XMLDocument();
    xmlPos = new tinyxml2::XMLDocument();
    xmlUsr = new tinyxml2::XMLDocument();
    xmlQst = new tinyxml2::XMLDocument();
    xmlInv->LoadFile(DATA_INV_PATH);
    xmlPos->LoadFile(DATA_POS_PATH);
    xmlUsr->LoadFile(DATA_USR_PATH);
    xmlQst->LoadFile(DATA_QST_PATH);

    rootInv = xmlInv->FirstChildElement("data");
    rootPos = xmlPos->FirstChildElement("data")->FirstChildElement("p");
    rootUsr = xmlUsr->FirstChildElement("data");
    rootQst = xmlQst->FirstChildElement("data");

    if (init || !isInitialized()) initDB();
}

DB::~DB() {
    delete xmlInv;
    delete xmlPos;
    delete xmlUsr;
    delete xmlQst;
}

void DB::initDB() {
    //init Inventory
    tinyxml2::XMLElement *node = rootInv->FirstChildElement("item");
    while (node != NULL) {
        if (node->IntAttribute("num") < 10) {
            node->SetAttribute("count", 1);
        }
        else {
            node->SetAttribute("count", 0);
        }
        node = node->NextSiblingElement();
    }
    xmlInv->SaveFile(DATA_INV_PATH);

    //Usr
    node = rootUsr->FirstChildElement("init");
    node->SetText(1);
    node = rootUsr->FirstChildElement("money");
    node->SetText(135000);
    node = rootUsr->FirstChildElement("time");
    node->SetText(0);
    xmlUsr->SaveFile(DATA_USR_PATH);

    //pos
    savePos(600, 600);

    //Qst
    node = rootQst->FirstChildElement("quest");
    while (node != NULL) {
        node->SetText(0);
        node = node->NextSiblingElement();
    }
    xmlInv->SaveFile(DATA_QST_PATH);
}

void DB::getPos(Map::Position &p) {
    p.x = rootPos->IntAttribute("x");
    p.y = rootPos->IntAttribute("y");
}


void DB::savePos(Map::Position p) {
    savePos(p.x, p.y);
}

void DB::savePos(int x, int y) {
    rootPos->SetAttribute("x", x);
    rootPos->SetAttribute("y", y);
    xmlPos->SaveFile(DATA_POS_PATH);
}

int DB::getItemCount(int num) {
    tinyxml2::XMLElement *node = rootInv->FirstChildElement("item");
    while (node->IntAttribute("num") != num) {
        node = node->NextSiblingElement();
    }
    return node->IntAttribute("count");
}

void DB::saveItemCount(int num, int count) {
    tinyxml2::XMLElement *node = rootInv->FirstChildElement("item");
    while (node->IntAttribute("num") != num) {
        node = node->NextSiblingElement();
    }
    node->SetAttribute("count", count);
    xmlInv->SaveFile(DATA_INV_PATH);
}

void DB::getInventory(std::vector<Item*>* &inv, std::map<int, Item*> *itemList) {
    inv->clear();

    tinyxml2::XMLElement *node = rootInv->FirstChildElement("item");
    do {
        int count = node->IntAttribute("count");
        if (count > 0) {
            int num = node->IntAttribute("num");

            Item *item = (*itemList)[num];
            item->setCount(count);
            inv->push_back(item);
        }
    } while ((node = node->NextSiblingElement()) != NULL);
}

void DB::saveInventory(std::vector<Item*> *inv){
    tinyxml2::XMLElement *node = rootInv->FirstChildElement("item"); //datafile - varience - conti
    std::vector<Item*>::iterator it; //inventory - fix - not conti
    int invNum;
    for (it = inv->begin(); it != inv->end(); it++) {
        invNum = (*it)->getNum();
        while (node->IntAttribute("num") != invNum) { node = node->NextSiblingElement(); };
        node->SetAttribute("count", (*it)->getCount());
    }
    xmlInv->SaveFile(DATA_INV_PATH);
}

bool DB::isInitialized() {
    tinyxml2::XMLElement *node = rootUsr->FirstChildElement("init");
    int data = atoi(node->GetText());
    return data;
}

void DB::setInitialized(int b) {
    tinyxml2::XMLElement *node = rootUsr->FirstChildElement("init");
    node->SetText(b);
    xmlUsr->SaveFile(DATA_USR_PATH);
}

int DB::getMoney() {
    tinyxml2::XMLElement *node = rootUsr->FirstChildElement("money");
    int data = atoi(node->GetText());
    return data;
}

void DB::setMoney(int m) {
    tinyxml2::XMLElement *node = rootUsr->FirstChildElement("money");
    node->SetText(m);
    xmlUsr->SaveFile(DATA_USR_PATH);
}

void DB::getQuests(std::vector<Quest*>* &quests, std::map<int, Quest*> *questList) {
    if(quests != NULL) quests->clear();

    tinyxml2::XMLElement *node = rootQst->FirstChildElement("quest");
    do {
        int state = atoi(node->GetText());
        if (state == Quest::ONGOING) {
            int num = node->IntAttribute("num");
            Quest *quest = (*questList)[num];
            quests->push_back(quest);
        }
    } while ((node = node->NextSiblingElement()) != NULL);
}

int DB::getQuestState(int num) {
    tinyxml2::XMLElement *node = rootQst->FirstChildElement("quest");
    while (node->IntAttribute("num") != num) {
        node = node->NextSiblingElement();
    }
    return atoi(node->GetText());
}

void DB::setQuestState(int num, int state) {
    tinyxml2::XMLElement *node = rootQst->FirstChildElement("quest");
    while (node->IntAttribute("num") != num) {
        node = node->NextSiblingElement();
    }
    node->SetText(state);
    xmlQst->SaveFile(DATA_QST_PATH);
}
