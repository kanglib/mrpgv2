//
// MRPGv2::Quest
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/13 RangeWING
//

#include "MRPG.h"

Quest::Quest(int num, Condition *startCond, Condition *endCond, std::string name, std::string msg, std::string descEnd, Reward *reward)
    : num(num), startCond(startCond), endCond(endCond), name(name), msg(msg), descEnd(descEnd), reward(reward)
{

}

Quest::~Quest() {

}

void Quest::start() {
}

void Quest::finish() {
}
