//
// MRPGv2::DB
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/12 RangeWING
//

#pragma once

class DB {
    Framework *framework;

    tinyxml2::XMLDocument *xmlInv, *xmlPos, *xmlUsr, *xmlQst;
    tinyxml2::XMLElement *rootInv, *rootPos, *rootUsr, *rootQst;
    const char DATA_INV_PATH[12] = "dat/inv.xml";
    const char DATA_POS_PATH[12] = "dat/pos.xml";
    const char DATA_USR_PATH[12] = "dat/x00.xml";
    const char DATA_QST_PATH[12] = "dat/qst.xml";

    void initDB();
    void setInitialized(int b);

public:
    DB(Framework *framework, bool init = false);
    ~DB();

    void getPos(Map::Position &p);
    void savePos(Map::Position p);
    void savePos(int x, int y);
    int getItemCount(int num);
    void saveItemCount(int num, int count);
    void getInventory(std::vector<Item*>* &inv, std::map<int, Item*> *itemList);
    void saveInventory(std::vector<Item*> *inv);
    bool isInitialized();
    int getMoney();
    void setMoney(int m);
    void getQuests(std::vector<Quest*>* &quests, std::map<int, Quest*> *questList);
    int getQuestState(int num);
    void setQuestState(int num, int state);
};
