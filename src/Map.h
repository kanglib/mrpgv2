//
// MRPGv2::Map
// Copyright (c) 2015 MR@KAIST
//chc
// 2015/10/31 kanglib
//

#pragma once

class Map {
public:
    struct RGB_Color { Uint8 r, g, b; };
    struct Position { int x, y; };
    Position Pos(int x, int y) { Position p; p.x = x; p.y = y;  return p; };
    struct Building { char *num; char *name; std::vector<Position> *pos; };
    Building BD(char *num, char *name, std::vector<Position> *pos) { Building bd; bd.num = num; bd.name = name; bd.pos = pos; return bd; }
    struct Mark { std::string bdNum; int mark; };
    const std::string mark_icon[2] = {"res/image/exclaim.png", "res/image/question.png"};

    enum { POS_UNDEFINED = -1, POS_NOT_ALLOWED, POS_ALLOWED, POS_WATER } posType;

    enum { NORTH, WEST, SOUTH, EAST } direction;

private:
    Framework* framework;

    SDL_Texture* tex;
    SDL_Surface* surf;
    int w, h;
    int x, y;
    std::vector<Mark*> *marks = NULL;

    std::map<std::string, Building> *buildings[4];
    std::vector<Position> *boundary_N;
    std::vector<Position> *boundary_WE;

    SDL_Texture *markTex[2];

public:
    Map(Framework* framework, std::string image);
    ~Map();

    Framework* getFramework() { return framework; }
    void getSize(int& w, int& h) { w = this->w; h = this->h; }

    void render(int x, int y);

    void initBld();
    void initArea();

    int codeToDistrict(const char code) {
        switch (code) {
        case 'N': return NORTH;
        case 'W': return WEST;
        case 'E': return EAST;
        default: return -1;
        }
    }

    char districtToCode(const int district) {
        switch (district) {
        case Map::NORTH: return 'N';
        case Map::WEST: return 'W';
        case Map::EAST: return 'E';
        default: return 'X';
        }
    }

    bool getBuilding(const char *num, Building &bd) {
        int d = codeToDistrict(num[0]);
        std::map<std::string, Building>::iterator it; it = buildings[d]->find(num);
        if (it == buildings[d]->end()) return false;
        else bd = it->second; return true;
    };

    void getPixel(int x, int y, RGB_Color *color);
    int getPosType(int x, int y);
    int getDistrict(int x, int y);

    Position centerOf(Building bd) {
        int X = 0, Y = 0, n = 0;
        std::vector<Position>::iterator it;
        for (it = bd.pos->begin(); it != bd.pos->end(); it++) {
            X += it->x;
            Y += it->y;
            n++;
        }
        return Pos(X / n, Y / n);
    }

    Position centerOf(const char *num) {
        Building bd;
        if(getBuilding(num, bd)) return centerOf(bd);
        else return Pos(-1, -1);
    }
    void setMarks(std::vector<Mark*> *marks) {
        this->marks = marks;
    }

    bool isInMaxSquare(int x, int y, std::vector<Position> *poly);
    bool isIn(int x, int y, std::vector<Position> *poly);

    bool findWhere(int x, int y, Building &bd);
};
