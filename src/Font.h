//
// MRPGv2::Font
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/01 kanglib
//

#pragma once

#define UINT16(str) reinterpret_cast<const Uint16*>(str)

class Font {
    Framework* framework;

    TTF_Font* fon;

public:
    enum MODE { SHADED, BLENDED };
    struct FORMAT_DATA { int money; };

    Font(Framework* framework, std::string ttf, int pt);
    ~Font();

    void render(std::string text, int x, int y, SDL_Color& fg);
    void render(const wchar_t *text, int x, int y, SDL_Color &fg);
    void render(std::string text, int x, int y, SDL_Color& fg, MODE mode);
    void render(const wchar_t *text, int x, int y, SDL_Color &fg, MODE mode);

    void encode(wchar_t* &dest, const char *src);
    int getTextWidth(const char *str);
    int getTextWidth(wchar_t *str);

    void format(wchar_t* &text, FORMAT_DATA data);
};
