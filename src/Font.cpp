//
// MRPGv2::Font
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/01 kanglib
//

#include "MRPG.h"

Font::Font(Framework* framework, std::string ttf, int pt) : framework(framework)
{
//  setlocale(LC_ALL, "Korean_Korea.949");
    fon = TTF_OpenFont(ttf.c_str(), pt);
    if (!fon) {
        framework->log("TTF_OpenFont");
        throw 1;
    }
}

Font::~Font()
{
    TTF_CloseFont(fon);
}

void Font::render(std::string text, int x, int y, SDL_Color &fg)
{
    render(text, x, y, fg, BLENDED);
}

void Font::render(std::string text, int x, int y, SDL_Color &fg, MODE mode)
{
    SDL_Surface* sur;
    SDL_Color white = { 255, 255, 255 };

    switch (mode) {
    case SHADED:
        sur = TTF_RenderText_Shaded(fon, text.c_str(), fg, white);
        if (!sur) {
            framework->log("TTF_RenderText_Shaded");
            throw 1;
        }
        break;
    case BLENDED:
        sur = TTF_RenderText_Blended(fon, text.c_str(), fg);
        if (!sur) {
            framework->log("TTF_RenderText_Blended");
            throw 1;
        }
        break;
    default:
        throw 1;
    }
    framework->render(sur, x, y);
    SDL_FreeSurface(sur);
}

void Font::render(const wchar_t *text, int x, int y, SDL_Color &fg)
{
    render(text, x, y, fg, BLENDED);
}

void Font::render(const wchar_t *text, int x, int y, SDL_Color &fg, MODE mode)
{
    SDL_Color white = { 255, 255, 255 };
    SDL_Surface* sur;
    switch (mode) {
    case SHADED:
        sur = TTF_RenderUNICODE_Shaded(fon, UINT16(text), fg, white);
        if (!sur) {
            framework->log("TTF_RenderUNICODE_Shaded");
            throw 1;
        }
        break;
    case BLENDED:
        sur = TTF_RenderUNICODE_Blended(fon, UINT16(text), fg);
        if (!sur) {
            framework->log("TTF_RenderUNICODE_Blended");
            throw 1;
        }
        break;
    default:
        throw 1;
    }

    framework->render(sur, x, y);
    SDL_FreeSurface(sur);
}

void Font::encode(wchar_t* &dest, const char *src) {
    int len = strlen(src);
    dest = new wchar_t[len + 1];
    mbstowcs(dest, src, len + 1);
}

int Font::getTextWidth(const char *str) {
    int w, h;
    if (!TTF_SizeText(fon, str, &w, &h)) return w;
    else return -1;
}

int Font::getTextWidth(wchar_t *str) {
    int w, h;
    if (!TTF_SizeUNICODE(fon, UINT16(str), &w, &h)) return w;
    else return -1;
}

void Font::format(wchar_t* &text, FORMAT_DATA data) {
    wstring str(text);
    int i = 0;
    size_t s, s2;
    while ((s = str.find(L"$(", i)) != std::wstring::npos) {
        s2 = str.find(L')', s + 2);
        wstring name = str.substr(s + 2, s2 - s - 2);
        if (name.compare(L"MONEY") == 0) {
            wchar_t money[10];
            swprintf(money, sizeof(money) / sizeof(wchar_t), L"%d", data.money);
            str.replace(s, s2 - s + 1, money);
        }
        i += 2;
    }
    wcscpy(text, str.c_str());
}
