﻿//
//  MRPGv2: Mister RPG in C++
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/26 kanglib
//

#include "MRPG.h"

enum {
    KEY_UP,
    KEY_LEFT,
    KEY_DOWN,
    KEY_RIGHT,
    KEY_R,
    KEY_SLASH,
    KEY_P,
    KEY_SPACE,
};

void Cleanup();
bool Handle(SDL_Event& e);
void HandleKeyDown(SDL_Keycode key);
void Render(SDL_Renderer* ren);
void EnterFrame(SDL_Renderer* ren);
void saveGame();
void action();
void actionRender();
void setDialog();

Framework* Game;
DB *db;
Font* Nanum12;
Font* Nanum20;
Map* KAIST;
User* Me;
bool Pressed[8];
bool viewInv = false, viewQst = false;
Item *sItem;
Map::Building *curBD = NULL;
CSerial serial;
char Buffer[BUFSIZ];

int main(int argc, char* argv[])
{
    std::string userName = "MR.Hubo", userColor = "gray";
    if (argc >= 2) serial.Open(atoi(argv[1]));
    if (argc >= 3) userName = argv[2];
    if (argc >= 4) userColor = argv[3];
    srand((unsigned)time(NULL));
    setlocale(LC_ALL, "Korean_Korea.949");
    try {
        Game = new Framework("MRPGv2 " VERSION " " BUILD " " __DATE__, 800, 480);
        db = new DB(Game);
        dialog = new Dialog(Game);
        Nanum12 = new Font(Game, "res/font/NanumBarunGothic.ttf", 12);
        Nanum20 = new Font(Game, "res/font/NanumBarunGothic.ttf", 20);
        KAIST = new Map(Game, "res/image/map.png");
        Me = new User(KAIST, db, userName, userColor);

        Me->checkQuests(*(new Quest::Condition()));

        Game->wndProc(Handle, Render, EnterFrame);
    } catch (int i) {
        Cleanup();
        return i;
    }
    Cleanup();
    return 0;
}

void Cleanup()
{
    delete dialog;
    delete Me;
    delete KAIST;
    delete Nanum20;
    delete Nanum12;
    delete db;
    delete Game;
}

bool Handle(SDL_Event& e)
{
    switch (e.type) {
    case SDL_QUIT:
        return false;
    case SDL_KEYDOWN:
        HandleKeyDown(e.key.keysym.sym);
        break;
    case SDL_KEYUP:
        if (e.key.keysym.sym == SDLK_SPACE) {
            Pressed[KEY_SPACE] = false;
        }
        break;
    }
    return true;
}

void HandleKeyDown(SDL_Keycode key) {
    float dx = 0;
    float dy = 0;
    float m = 1;

    if (key != SDLK_PASTE) {
        const Uint8 *keys = SDL_GetKeyboardState(NULL);
        if (keys[SDL_SCANCODE_UP]) dy--;
        if (keys[SDL_SCANCODE_DOWN]) dy++;
        if (keys[SDL_SCANCODE_LEFT]) dx--;
        if (keys[SDL_SCANCODE_RIGHT]) dx++;
        if (keys[SDL_SCANCODE_R]) m = 2;
        if (keys[SDL_SCANCODE_S]) saveGame();
        if (keys[SDL_SCANCODE_Q]) viewQst = !viewQst;
        if (keys[SDL_SCANCODE_ESCAPE]) { saveGame(); throw 0; }
        if (keys[SDL_SCANCODE_I]) {
            viewInv = !viewInv; sItem = NULL;
            if (viewInv) dialog->setMessage(L"Space bar를 눌러 아이템을 선택하세요.");
            else dialog->setMessage(L" ");
        }
        if (keys[SDL_SCANCODE_SPACE]) { action(); Pressed[KEY_SPACE] = true; }
        else Pressed[KEY_SPACE] = false;
        if (keys[SDL_SCANCODE_T]) dialog->setMessage(L"안녕하세요. ㅎㅎ");

        //command key
        if (keys[SDL_SCANCODE_SLASH]) {
            if (keys[SDL_SCANCODE_R]) m = 4;
            if (keys[SDL_SCANCODE_P]) {
                int x, y;
                Me->getPosition(x, y);
                cout << "<p x=\"" << x << "\" y=\"" << y << "\" />" << endl;
            }
            if (keys[SDL_SCANCODE_C]) {
                int x, y;
                Me->getPosition(x, y);
                Map::RGB_Color color;
                KAIST->getPixel(x, y, &color);
                cout << "color = (" << (int)color.r << ", " << (int)color.g << ", " << (int)color.b << ")" << endl;
            }
            if (keys[SDL_SCANCODE_A]) {
                Me->addItem(100, 1);
                dialog->setMessage(L"/A");
            }
        }
    } else {
        static int x = 0;
        static int y = 0;
        static int p = 0;
        static int v = 0;
        if (serial.ReadData(Buffer, sizeof(Buffer))) {
            sscanf(Buffer, "%d%d%d%d", &x, &y, &p, &v);
            switch (p) {
            case 1:
                viewQst = !viewQst;
                break;
            case 2:
                saveGame();
                break;
            case 3:
                viewInv = !viewInv; sItem = NULL;
                if (viewInv) dialog->setMessage(L"A를 눌러 아이템을 선택하세요.");
                else dialog->setMessage(L" ");
                break;
            case 4:
                action();
                break;
            }
            Pressed[KEY_SPACE] = p == 4;
        } else {
            dx = (float) x / 10;
            dy = (float) -y / 10;
        }
    }

    if (dx != 0 && dy != 0) {
        dx /= 1.414214f;
        dy /= 1.414214f;
    }
    Me->move(dx, dy, m);
}

void Render(SDL_Renderer* ren)
{
    int ws;
    int hs;
    Game->getScreenSize(ws, hs);

    int wm;
    int hm;
    KAIST->getSize(wm, hm);

    int x;
    int y;
    Me->getPosition(x, y);

    int xk;
    int yk;
    int xm;
    int ym;
    if (x < ws / 2) {
        xk = 0;
        xm = x;
    } else if (x >= wm - ws / 2) {
        xk = wm - ws - 1;
        xm = x - xk;
    } else {
        xk = x - ws / 2;
        xm = ws / 2;
    }
    if (y < hs / 2) {
        yk = 0;
        ym = y;
    } else if (y >= hm - hs / 2) {
        yk = hm - hs - 1;
        ym = y - yk;
    } else {
        yk = y - hs / 2;
        ym = hs / 2;
    }
    KAIST->render(xk, yk);

    Me->render(xm, ym);
    static SDL_Color black = { 0, 0, 0 };
    static SDL_Color gray = { 64, 64, 64 };
    wchar_t *encodedName;
    Nanum12->encode(encodedName, Me->getName().c_str());
    int nameWidth = Nanum12->getTextWidth(encodedName);
    Nanum12->render(encodedName, xm-nameWidth/2, ym - 42, black);

    char text_fps[9];
    char text_pos[15];
    char text_buffer_loc[24];
    wchar_t *text_location;
    char text_stamina[9];

    char district = KAIST->districtToCode(KAIST->getDistrict(x, y));
    Map::Building bd;
    if (curBD) delete curBD;
    if (KAIST->findWhere(x, y, bd)) {
        snprintf(text_buffer_loc, sizeof(text_buffer_loc), "%s %s", bd.num, bd.name);
        curBD = new Map::Building(bd);
    }
    else {
        snprintf(text_buffer_loc, sizeof(text_buffer_loc), "%c", district);
        curBD = NULL;
    }
    Nanum12->encode(text_location, text_buffer_loc);

    snprintf(text_fps, sizeof(text_fps), "%d fps", Game->getFPS());
    snprintf(text_pos, sizeof(text_pos), "(%d, %d)", x, y);
    snprintf(text_stamina, sizeof(text_stamina), "%2.0f/%2.0f", Me->getStamina(), Me->maxStamina());

    Nanum12->render(text_fps, 10, 10, gray);
    Nanum12->render(text_pos, 10, 22, gray);
    Nanum12->render(text_location, 10, 34, gray);
    Nanum12->render(text_stamina, 10, 46, gray);
    if (viewInv) {
        Game->render(Me->getInvTex(), 180, 120);
        if (sItem != NULL) {
            Font::FORMAT_DATA data;
            data.money = Me->getMoney();
            sItem->render(data);
        }
    }

    if (viewQst) {
        Me->renderQuests();
    }

    dialog->render();
    Nanum12->render("Map by Kakao, Character from CS101", 580, 465, gray);
    if (Pressed[KEY_SPACE]) {
        actionRender();
    }
}

void EnterFrame(SDL_Renderer* ren) {
    if (Me->movingState() <= User::WALKING) {
        Me->recoverStamina(1.5f / MAX_FPS);
    }
    Me->resetState();
    setDialog();
}

void saveGame() {
    db->savePos(Me->getPosition());
    db->saveInventory(Me->getInventory());
    dialog->setMessage(L"게임이 저장되었습니다.");
}

void action() {
    if (viewInv) { Me->extractItem(sItem); return; }
    Quest::Condition *cond = new Quest::Condition();
    if(curBD) cond->bdNum.assign(curBD->num);
    Me->checkQuests(*cond);
    delete cond;
}

void actionRender() {
    SDL_Color black = { 0, 0, 0 };
    if (curBD && strcmp(curBD->num, "N16-1") == 0) {
        SDL_Texture *pad = Game->loadTexture("res/image/notepad.png");
        Game->render(pad, 180, 60);
        Font *Nanum15 = new Font(Game, "res/font/NanumBarunGothic.ttf", 15);
        Font *NanumC11 = new Font(Game, "res/font/NanumGothicCoding.ttf", 11);
        Nanum20->render(L"MRPG 정보", 220, 75, black);
        Nanum12->render(L"Copyright (c) 2015 KAIST MR", 220, 99, black);
        Nanum12->render(L"Microrobot Research 15", 220, 112, black);
        Nanum15->render(L"-Team MRPG", 220, 131, black);
        NanumC11->render(L"Developer \tRangeWING", 230, 152, black);
        NanumC11->render(L"Developer \tkanglib", 230, 166, black);
        NanumC11->render(L"Designer \t\tKMini", 230, 179, black);
        NanumC11->render(L"Co-worker \tJCH", 230, 192, black);
        Nanum15->render(L"-Contributors", 220, 211, black);
        Nanum12->render(L"DUCK_U, KMini, 김현준, 편재훈", 230, 232, black);
        Nanum12->render(L"감사합니다!!!", 230, 246, black);
        SDL_DestroyTexture(pad);
    }
}

void setDialog() {
    if (curBD == NULL || curBD->num == NULL) return;
    if (strcmp(curBD->num, "N16-1") == 0) {
        dialog->setMessage(L"Space Bar를 눌러서 게임 정보 보기");
    } else if (strcmp(curBD->num, "N13") == 0) {
        dialog->setMessage(L"Space Bar를 눌러서 동방 방문");
    }
}
