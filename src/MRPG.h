//
//  MRPGv2: Mister RPG in C++
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/29 kanglib
//

#pragma once

#define MAX_FPS 30
#define BUILD "FB146"
#define VERSION "Beta 0.2"
#define DEBUG_MODE

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <Windows.h>
#include <Serial.h>
#include <tinyxml2.h>

#pragma warning(disable:4996)
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <utility>
#include <vector>

using namespace std;

#include "Framework.h"
#include "Font.h"
#include "Item.h"
#include "Map.h"
#include "Quest.h"
#include "DB.h"
#include "Dialog.h"
#include "User.h"

static Dialog *dialog;
