//
// MRPGv2::User
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/26 kanglib
//

#include "MRPG.h"

User::User(Map* map, DB *db, std::string name, std::string color)
    : map(map), db(db), name(name), color(color), speed(5),
    direction(DOWN), stamina(10), MAX_STAMINA(10), movState(STOP)
{
    const char pathDir[4][6] = {"up", "left", "down", "right"};
    Map::Position p;
    db->getPos(p);
    money = db->getMoney();
    x = p.x; y = p.y;

    char path[36];
    for (int i = 0; i < 4; i++) {
        snprintf(path, sizeof(path), "res/image/robot_%s_%s.jpg", color.c_str(), pathDir[i]);
        tex[i] = map->getFramework()->loadTexture(path);
    }

    invTex = map->getFramework()->loadTexture("res/image/inventory.png");
    inventory = new vector<Item*>();
    quests = new vector<Quest*>();
    initItems();
    initQuests();
}

User::~User()
{
    for (int i = 0; i < 4; i++) {
        SDL_DestroyTexture(tex[i]);
    }
    delete inventory;
}

void User::setSpeed(int speed)
{
    this->speed = speed;
}

void User::render(int x, int y)
{
    map->getFramework()->render(tex[direction], x - 11, y - 27);
}

void User::move(float dx, float dy, float m)
{
    if (m > 1 && stamina <= 0) m = 1.01f; //intended number
    if (map->getPosType(x, y) == Map::POS_WATER) {
        movState = SWIMMING;
        m /= 4;
    }

    int w, h;
    int X = x, Y = y;
    map->getSize(w, h);

    if (dx != 0) {
        if (dx < 0) {
            direction = LEFT;
        }
        else if (dx > 0) {
            direction = RIGHT;
        }
        X = x + (int)(dx * speed * m);
        if (X < 11) {
            X = 11;
        }
        else if (X >= w - 11) {
            X = w - 12;
        }
    }

    if (dy != 0) {
        if (dy < 0) {
            direction = UP;
        }
        else if (dy > 0) {
            direction = DOWN;
        }

        Y = y + (int)(dy * speed * m);
        if (Y < 15) {
            Y = 15;
        }
        else if (Y >= h - 15) {
            Y = h - 16;
        }
    }
    switch (map->getPosType(X, Y)) {
    case Map::POS_WATER:
        movState = SWIMMING;
        if (stamina <= 0) return;
        stamina -= 3.0f / MAX_FPS;
    case Map::POS_ALLOWED:
        x = X; y = Y;
        if (m > 1) {
            if(stamina >= 3.0f/MAX_FPS) stamina -= 3.0f / MAX_FPS;
            else stamina = 0;
            movState = RUNNING;
        }
        else if (m == 1) movState = WALKING;
    }
}

void User::initItems() {
    itemList = new std::map<int, Item*>();

    tinyxml2::XMLDocument xml;
    xml.LoadFile("res/xml/items.xml");

    tinyxml2::XMLElement *root = xml.FirstChildElement("data");
    tinyxml2::XMLElement *itemNode, *node;

    itemNode = root->FirstChildElement("item");

    int num, price;
    std::string name, path, desc;

    do {
        num = itemNode->IntAttribute("num");
        node = itemNode->FirstChildElement("name");
        name = node->GetText();
        node = itemNode->FirstChildElement("price");
        price = atoi(node->GetText());
        node = itemNode->FirstChildElement("path");
        path = node->GetText();
        node = itemNode->FirstChildElement("desc");
        desc = node->GetText();
        Item *item = new Item(num, -1, name, price, path, desc, map->getFramework());
        (*itemList)[num] = item;
    } while ((itemNode = itemNode->NextSiblingElement()) != NULL);

    db->getInventory(inventory, itemList);
}

bool User::extractItem(Item* &item) {
    if (inventory->size() > 0) {
        int idx, except;
        if (item != NULL) {
            except = item->getNum();
            do {
                idx = rand() % inventory->size();
                item = (*inventory)[idx];
            } while (item->getNum() == except);
        }
        else {
            idx = rand() % inventory->size();
        }
        item = (*inventory)[idx];
        return true;
    }
    else {
        item = NULL;
        return false;
    }
}

bool User::addItem(int num, int count) {
    Item *item = (*itemList)[num];
    vector<Item*>::iterator first = inventory->begin();
    vector<Item*>::iterator last = inventory->end();
    vector<Item*>::iterator it;
    while (first != last) {
        if (*first == item) {
            it = first;
            goto exit;
        }
        first++;
    }
    it = last;

exit:
    if (it != inventory->end()) {
        (*it)->setCount((*it)->getCount()+count);
        return true;
    }
    else {
        item->setCount(count);
        inventory->push_back(item);
        return false;
    }
}

void User::initQuests() {
    questList = new std::map<int, Quest*>();

    tinyxml2::XMLDocument xml;
    xml.LoadFile("res/xml/quests.xml");

    tinyxml2::XMLElement *root = xml.FirstChildElement("data");
    tinyxml2::XMLElement *questNode, *node;

    questNode = root->FirstChildElement("quest");

    int num;
    Quest::Condition *start, *end;
    std::string name, msg, descEnd;
    Quest::Reward *reward;

    do {
        num = questNode->IntAttribute("num");
        node = questNode->FirstChildElement("name");
        name = node->GetText();
        node = questNode->FirstChildElement("desc")->FirstChildElement("msg");
        msg = node->GetText();
        node = questNode->FirstChildElement("desc")->FirstChildElement("end");
        descEnd = node->GetText();

        start = new Quest::Condition();
        end = new Quest::Condition();

        node = questNode->FirstChildElement("start")->FirstChildElement("cond");
        do {
            std::string type = node->Attribute("type");
            if (type.compare("date") == 0) { start->date = atoi(node->GetText()); }
            else if (type.compare("level") == 0) { start->level = atoi(node->GetText()); }
            else if (type.compare("building") == 0) { start->bdNum = node->GetText(); }
        } while ((node = node->NextSiblingElement()) != NULL);

        node = questNode->FirstChildElement("end")->FirstChildElement("cond");
        do {
            std::string type = node->Attribute("type");
            if (type.compare("date") == 0) { end->date = atoi(node->GetText()); }
            else if (type.compare("level") == 0) { end->level = atoi(node->GetText()); }
            else if (type.compare("building") == 0) { end->bdNum = node->GetText(); }
        } while ((node = node->NextSiblingElement()) != NULL);

        reward = new Quest::Reward();
        node = questNode->FirstChildElement("end")->FirstChildElement("reward");
        do {
            std::string type = node->Attribute("type");
            if (type.compare("item") == 0) {
                reward->item = new Item(*(*itemList)[node->IntAttribute("num")]);
            }
        } while ((node = node->NextSiblingElement()) != NULL);

        Quest *quest = new Quest(num, start, end, name, msg, descEnd, reward);
        (*questList)[num] = quest;
    } while ((questNode = questNode->NextSiblingElement()) != NULL);

    db->getQuests(quests, questList);
}

void User::checkQuests(Quest::Condition now) {
    map->setMarks(getQuestPlaces(now));
    std::map<int, Quest*>::iterator it;
    for (it = questList->begin(); it != questList->end(); it++) {
        Quest *quest = it->second;
        Quest::Condition cond;
        bool met = true;
        int state = db->getQuestState(quest->getNum());
        switch (state) {
        case Quest::CLOSED:
            cond = quest->getStartCond();
            break;
        case Quest::ONGOING:
            cond = quest->getEndCond();
            break;
        default:
            continue;
        }
        if (strcmp(cond.bdNum.c_str(), "NULL") != 0 && strcmp(cond.bdNum.c_str(), now.bdNum.c_str()) != 0) {
            met = false;
        }
        if (now.date < cond.date) met = false;
        if (now.level < cond.level) met = false;
        if (met) {
            if (state == Quest::CLOSED) {
                quest->start();
                //dialog->setMessage(L"퀘스트를 받았습니다.");
                db->setQuestState(quest->getNum(), Quest::ONGOING);
                quests->push_back(quest);
            }
            else if (state == Quest::ONGOING) {
                quest->finish();
                //dialog->setMessage(L"퀘스트를 완료했습니다.");
                db->setQuestState(quest->getNum(), Quest::DONE);
                for (std::vector<Quest*>::iterator it = quests->begin(); it != quests->end(); it++) {
                    if ((*it) == quest) {
                        quests->erase(it);
                        break;
                    }
                }
            }
        }
    }
}

void User::renderQuests() {
    Framework *framework = map->getFramework();
    SDL_Texture *pad = framework->loadTexture("res/image/notepad.png");
    Font *Nanum20 = new Font(framework, "res/font/NanumBarunGothic.ttf", 20);
    Font *Nanum12 = new Font(framework, "res/font/NanumBarunGothic.ttf", 12);

    framework->render(pad, 110, 60);
    //framework->render(pad, 410, 60);
    SDL_Color black = { 0, 0, 0 };

    wchar_t *encoded;
    Nanum20->encode(encoded, "Quest");
    Nanum20->render(encoded, 145, 80, black);

    int n = 0;
    int i = 0;
    vector<Quest*>::iterator it;
    vector<std::string> *desc = new vector<std::string>();
    for (it = quests->begin(); it != quests->end(); it++) {
        desc->clear();
        char buffer[256];
        sprintf(buffer, "%d. %s", ++n, (*it)->getName().c_str());
        Nanum12->encode(encoded, buffer);
        Nanum12->render(encoded, 145, 100 + 13 * (i++), black);

        sprintf(buffer, "설명: %s", (*it)->getMessage().c_str());
        std::string description = buffer;
        int j = -1;
        int pos = description.find("\\n");
        if (pos > 0) {
            while (pos != string::npos) {
                desc->push_back(description.substr(j + 1, pos -j - 1));
                j = ++pos;
                pos = description.find("\\n", pos + 1);
                if (pos == string::npos) {
                    desc->push_back(description.substr(j + 1, description.length()));
                }
            }
        }
        else {
            desc->push_back(description);
        }
        vector<std::string>::iterator it2;
        for (it2 = desc->begin(); it2 != desc->end(); it2++) {
            Nanum12->encode(encoded, (*it2).c_str());
            Nanum12->render(encoded, 145, 100 + 13 * (i++), black);
        }

        sprintf(buffer, "완수 조건: %s", (*it)->getDescEnd().c_str());
        Nanum12->encode(encoded, buffer);
        Nanum12->render(encoded, 145, 100 + 13 * (i++), black);
        i++;

    }

    free(encoded);
    delete Nanum12;
    delete Nanum20;
    delete desc;
    SDL_DestroyTexture(pad);
}

std::vector<Map::Mark*>* User::getQuestPlaces(Quest::Condition now) {
    std::vector<Map::Mark*> *marks = new std::vector<Map::Mark*>();

    std::map<int, Quest*>::iterator it;
    for (it = questList->begin(); it != questList->end(); it++) {
        Quest *quest = it->second;
        Quest::Condition cond;
        int state = db->getQuestState(quest->getNum());
        switch (state) {
        case Quest::CLOSED:
            cond = quest->getStartCond();
            break;
        case Quest::ONGOING:
            cond = quest->getEndCond();
            break;
        }
        bool met = true;
        if (strcmp(cond.bdNum.c_str(), "NULL") != 0 && now.date >= cond.date && now.level >=cond.level) {
            Map::Mark *mark = new Map::Mark();
            mark->bdNum = cond.bdNum;
            mark->mark = state;
            marks->push_back(mark);
        }
    }
    return marks;
}
