﻿//
// MRPGv2::Dialog
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/10 RangeWING
//

#include "MRPG.h"

Dialog::Dialog(Framework *framework) : framework(framework) {
    font = new Font(framework, "res/font/NanumBarunGothic.ttf", 15);
    wcscpy(msg, L"안녕하세요.");
}

Dialog::~Dialog() {
    delete font;
}

void Dialog::setMessage(wchar_t *msg) {
    wcscpy(this->msg, msg);
}

void Dialog::render() {
    SDL_Color black = {0, 0, 0};
    font->render(msg, 1, 460, black, font->SHADED);
}
