//
// MRPGv2::Map
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/31 kanglib
//

#include "MRPG.h"

Map::Map(Framework* framework, std::string image) : framework(framework)
{
    tex = framework->loadTexture(image);
    surf = IMG_Load(image.c_str());
    SDL_QueryTexture(tex, NULL, NULL, &w, &h);
    initBld();
    initArea();
    markTex[0] = framework->loadTexture(mark_icon[0]);
    markTex[1] = framework->loadTexture(mark_icon[1]);
}

Map::~Map()
{
    SDL_DestroyTexture(tex);
    SDL_DestroyTexture(markTex[0]);
    SDL_DestroyTexture(markTex[1]);
}

void Map::render(int x, int y)
{
    int w;
    int h;
    framework->getScreenSize(w, h);

    SDL_Rect clip = { x, y, w, h };
    framework->render(tex, 0, 0, &clip);

    if (marks != NULL) {
        std::vector<Mark*>::iterator it;
        for (it = marks->begin(); it != marks->end(); it++) {
            Position p = centerOf((*it)->bdNum.c_str());
            framework->render(markTex[(*it)->mark], p.x-x-15, p.y-y-15);
        }
    }
}

void Map::initBld() {
    buildings[NORTH] = new std::map<std::string, Building>();
    buildings[WEST] = new std::map<std::string, Building>();
    buildings[EAST] = new std::map<std::string, Building>();

    char *num, *name;
    char x[5], y[5];
    std::vector<Position> *bd;

    tinyxml2::XMLDocument xml;
    xml.LoadFile("res/xml/buildings.xml");

    tinyxml2::XMLElement *root = xml.FirstChildElement("data");
    tinyxml2::XMLElement *bdNode, *aNode;
    tinyxml2::XMLElement *pNode;

    aNode = root->FirstChildElement("area");
    do {
        int district = codeToDistrict(aNode->Attribute("name")[0]);
        bdNode = aNode->FirstChildElement("building");
        do {
            num = (char *)malloc(sizeof(char) * 6);
            strcpy(num, bdNode->Attribute("num"));
            name = (char *)malloc(sizeof(char) * 24);
            strcpy(name, bdNode->Attribute("name"));

            pNode = bdNode->FirstChildElement("p");
            bd = new std::vector<Position>;
            do {
                strcpy(x, pNode->Attribute("x"));
                strcpy(y, pNode->Attribute("y"));
                bd->push_back(Pos(atoi(x), atoi(y)));
            } while ((pNode = pNode->NextSiblingElement()) != NULL);
            //buildings.insert(std::pair<std::string, Building>(std::string(name), BD(num, name, bd)));
            (*buildings[district])[num] = BD(num, name, bd);
        } while ((bdNode = bdNode->NextSiblingElement()) != NULL);
    } while ((aNode = aNode->NextSiblingElement()) != NULL);
}

void Map::initArea() {
    char x[5], y[5];

    tinyxml2::XMLDocument xml;
    xml.LoadFile("res/xml/districts.xml");

    tinyxml2::XMLElement *root = xml.FirstChildElement("data");
    tinyxml2::XMLElement *pNode, *aNode;

    boundary_N = new std::vector<Position>();
    boundary_WE = new std::vector<Position>();

    aNode = root->FirstChildElement("area");

    pNode = aNode->FirstChildElement("p");
    do {
        strcpy(x, pNode->Attribute("x"));
        strcpy(y, pNode->Attribute("y"));
        boundary_N->push_back(Pos(atoi(x), atoi(y)));
    } while ((pNode = pNode->NextSiblingElement()) != NULL);

    pNode = aNode->NextSiblingElement()->FirstChildElement("p");
    do {
        strcpy(x, pNode->Attribute("x"));
        strcpy(y, pNode->Attribute("y"));
        boundary_WE->push_back(Pos(atoi(x), atoi(y)));
    } while ((pNode = pNode->NextSiblingElement()) != NULL);
}

void Map::getPixel(int x, int y, RGB_Color *color) {
    int bpp = surf->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surf->pixels + y * surf->pitch + x * bpp;

    Uint32 pixel;

    switch (bpp) {

    case 1:
        pixel = *p;
        break;

    case 2:
        pixel = *(Uint16 *)p;
        break;

    case 3:
        if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
            pixel = p[0] << 16 | p[1] << 8 | p[2];
        else
            pixel = p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        pixel = *(Uint32 *)p;
        break;

    default:
        throw 1;
    }

    SDL_GetRGB(pixel, surf->format, &color->r, &color->g, &color->b);
}

int Map::getPosType(int x, int y) {
    RGB_Color color;
    getPixel(x, y, &color);
    if (color.r == 0 && color.g == 0 && color.b == 0) {
        return POS_UNDEFINED;
    }
    else if (color.r == 212 && color.g == 219 && color.b == 218) {
        return POS_NOT_ALLOWED;
    }
    else if (color.r == 146 && color.g == 185 && color.b == 211) {
        return POS_WATER;
    }
    else {
        return POS_ALLOWED;
    }
}

int Map::getDistrict(int x, int y) {
    int i = 0;
    vector<Position>::iterator iter;
    for (iter = boundary_N->begin(); iter != boundary_N->end(); iter++) {
        if (x < (*iter).x) break;
        i++;
    }
    // HACK HACK HACK
    if (i == boundary_N->size()) {
        i--;
    }
    Position p1 = (*boundary_N)[i - 1];
    Position p2 = (*boundary_N)[i];

    if (y < p1.y + (float)(p2.y - p1.y) / (p2.x - p1.x) * (x - p1.x)) return NORTH;
    i = 0;
    for (iter = boundary_WE->begin(); iter != boundary_WE->end(); iter++) {
        if (y < (*iter).y) break;
        i++;
    }
    // HACK HACK HACK
    if (i == boundary_WE->size()) {
        i--;
    }
    p1 = (*boundary_WE)[i - 1];
    p2 = (*boundary_WE)[i];
    if (x < p1.x + (float)(p2.x - p1.x) / (p2.y - p1.y) * (y - p1.y)) return WEST;
    else return EAST;
}

bool Map::isInMaxSquare(int x, int y, std::vector<Position> *poly) {
    int mx = 3000, my = 3000;
    int MX = 0, MY = 0;
    for (size_t i = 0; i < poly->size(); i++) {
        Position p = (*poly)[i];
        if (mx > p.x) mx = p.x;
        if (my > p.y) my = p.y;
        if (MX < p.x) MX = p.x;
        if (MY < p.y) MY = p.y;
    }
    if (x > mx && x < MX && y > my && y < MY) return true;
    else return false;
}

bool Map::isIn(int x, int y, std::vector<Position> *poly) {
    //int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
    bool c = false;
    int i, j;
    int nvert = poly->size();
    for (i = 0, j = nvert - 1; i < nvert; j = i++) {
        if ((((*poly)[i].y>y) != ((*poly)[j].y>y)) &&
            (x < ((*poly)[j].x - (*poly)[i].x) * (y - (*poly)[i].y) / ((*poly)[j].y - (*poly)[i].y) + (*poly)[i].x))
            c = !c;
    }
    return c;
}

bool Map::findWhere(int x, int y, Building &bd) {
    int district = getDistrict(x, y);
    std::map<std::string, Building>::iterator iter;
    for (iter = buildings[district]->begin(); iter != buildings[district]->end(); iter++) {
        std::vector<Position> *p = (*iter).second.pos;
        if (isIn(x, y, p)) { bd = (*iter).second; return true; }
    }
    return false;
}
