//
// MRPGv2::Framework
// Copyright (c) 2015 MR@KAIST
//
// 2015/10/26 kanglib
//

#pragma once

class Framework {
    int w;
    int h;
    int fps;

    SDL_Window* win;
    SDL_Renderer* ren;

public:
    typedef bool (*EVENT_HANDLER)(SDL_Event&);
    typedef void (*RENDERER)(SDL_Renderer*);
    typedef void (*ENTER_FRAME)(SDL_Renderer*);

    Framework(std::string title, int w, int h);
    ~Framework();

    SDL_Window* getWindow() { return this->win;  }
    void getScreenSize(int& w, int& h) { w = this->w; h = this->h; }
    int getFPS() { return fps; }

    void log(std::string tag);
    void wndProc(EVENT_HANDLER handle, RENDERER render, ENTER_FRAME enterFrame);

    SDL_Texture* loadTexture(std::string image);
    void render(SDL_Texture* tex, int x, int y, SDL_Rect* clip = NULL);
    void render(SDL_Surface* sur, int x, int y, SDL_Rect* clip = NULL);
};
