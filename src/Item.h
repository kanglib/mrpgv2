//
// MRPGv2::Item
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/11 kanglib
//

#pragma once

class Item {
    Framework *framework;
    int num, count;
    std::string name;
    int price;
    std::string imagePath;
    std::string description;

public:
    Item(int num, int count, std::string name, int price, std::string imagePath, std::string description, Framework *framework);
    void render(Font::FORMAT_DATA data);
    void render() { Font::FORMAT_DATA nullData; render(nullData); }
    int getNum() { return num; }
    int getCount() { return count; }
    void setCount(int count) { this->count = count; }
};
