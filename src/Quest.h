//
// MRPGv2::Quest
// Copyright (c) 2015 MR@KAIST
//
// 2015/11/13 RangeWING
//

#pragma once

class Quest {
public:
    struct Condition {
        int date = 0;
        int level = 0;
        std::string bdNum = "NULL";
    };
    struct Reward {
        int money = 0;
        int exp = 0;
        Item *item = NULL;
    };
    enum { CLOSED=0, ONGOING=1, DONE=2 };
private:
    int num;
    Condition *startCond, *endCond;
    std::string name, msg, descEnd;
    Reward *reward;

public:
    Quest(int num, Condition *startCond, Condition *endCond, std::string name, std::string msg, std::string descEnd, Reward *reward);
    ~Quest();
    void render();
    Condition getStartCond() { return *startCond; }
    Condition getEndCond() { return *endCond; }
    std::string getName() { return name; }
    std::string getMessage() { return msg; }
    std::string getDescEnd() { return descEnd; }
    int getNum() { return num; }
    void start();
    void finish();
};
